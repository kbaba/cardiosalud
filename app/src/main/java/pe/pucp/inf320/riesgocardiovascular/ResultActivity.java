package pe.pucp.inf320.riesgocardiovascular;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by Kevin on 23-May-15.
 */
public class ResultActivity extends ActionBarActivity {
    private Button btnVolver;
    private Button btnMail;
    private TextView txtImc;
    private TextView txtRieCar;
    private TextView txtRieHip;
    private TextView txtNivel;
    private ImageView imgNivel;
    final Context context = this;
    private static final String username = "cardiosaludmobile@gmail.com";
    private static final String password = "a20101812";
    /*
    private Session createSessionObject() {
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "465");

        return Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
    }

    private Message createMessage(String email, String subject, String messageBody, Session session) throws MessagingException, UnsupportedEncodingException {
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("tutorials@tiemenschut.com", "Tiemen Schut"));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email, email));
        message.setSubject(subject);
        message.setText(messageBody);
        return message;
    }

    private void sendMail(String email, String subject, String messageBody) {
        Session session = createSessionObject();

        try {
            Message message = createMessage(email, subject, messageBody, session);
            new SendMailTask().execute(message);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private class SendMailTask extends AsyncTask<Message, Void, Void> {
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ResultActivity.this, "Por favor espere", "Enviando correo", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_screen);

        btnVolver = (Button) findViewById(R.id.btnVolver);
        btnMail = (Button) findViewById(R.id.btnMail);
        txtImc = (TextView) findViewById(R.id.txtImc);
        txtRieCar = (TextView) findViewById(R.id.txtRieCar);
        txtRieHip = (TextView) findViewById(R.id.txtRieHip);
        txtNivel = (TextView) findViewById(R.id.lblNivel);
        imgNivel = (ImageView) findViewById(R.id.imgNivel);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            txtImc.setText(extras.getString("imc") + " kg/m²");
            txtRieCar.setText(extras.getString("rieCar")+"%");
            txtRieHip.setText(extras.getString("rieHip")+"%");
        }
        Double imc = new Double(extras.getString("imc"));
        Double car = new Double(extras.getString("rieCar"));
        Double hip = new Double(extras.getString("rieHip"));
        int contador = 0;
        if (imc < 18.5 || imc > 25){
            txtImc.setTextColor(Color.parseColor("#FF0000"));
            contador++;
        }
        if (car > 7){
            txtRieCar.setTextColor(Color.parseColor("#FF0000"));
            contador++;
        }
        if (hip > 7){
            txtRieHip.setTextColor(Color.parseColor("#FF0000"));
            contador++;
        }

        if (contador==0){
            txtNivel.setText(R.string.bajo);
            imgNivel.setImageResource(R.drawable.traffic_green);
        }
        else{
            if (contador==1){
                txtNivel.setText(R.string.medio);
                imgNivel.setImageResource(R.drawable.traffic_yellow);
            }
            else{
                txtNivel.setText(R.string.alto);
                imgNivel.setImageResource(R.drawable.traffic_red);
            }
        }

        //pb = (ProgressBar) findViewById(R.id.progressBar);

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //pb.setVisibility(View.VISIBLE);
                Intent i = new Intent("pe.pucp.inf320.riesgocardiovascular.MAINACTIVITY");
                startActivity(i);
            }
        });

        btnMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.prompt, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInput = (EditText) promptsView
                        .findViewById(R.id.editTextDialogUserInput);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Enviar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // get user input and set it to result
                                        // edit text
                                        //result.setText(userInput.getText());

                                        String cuerpo = "Estimado usuario,\nSu índice de masa corporal es de " + txtImc.getText().toString() + "\n";
                                        cuerpo = cuerpo + "Su riesgo de tener una enfermedad cardiovascular en 10 años es de " + txtRieCar.getText().toString() + "%\n";
                                        cuerpo = cuerpo + "Su riesgo de tener hipertensión en 4 años es de " + txtRieCar.getText().toString() + "%\n";
                                        cuerpo = cuerpo + "Saludos coordiales.";
                                        System.out.println(userInput.getText().toString());

                                        Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                                        intent.setType("text/plain");
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "CardioSalud - Riesgo Cardiovascular");
                                        intent.putExtra(Intent.EXTRA_TEXT, cuerpo);
                                        intent.setData(Uri.parse("mailto:"+userInput.getText().toString())); // or just "mailto:" for blank
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                                        try {
                                            startActivity(intent);
                                        } catch (Exception ex) {
                                            Toast.makeText(ResultActivity.this, "No se pudo enviar el correo a la dirección indicada.", Toast.LENGTH_SHORT).show();
                                        }
                                        //sendMail(userInput.getText().toString(), "CardioSalud - Riesgo Cardiovascular", cuerpo);

                                    }
                                })
                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up btnIngresar, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
