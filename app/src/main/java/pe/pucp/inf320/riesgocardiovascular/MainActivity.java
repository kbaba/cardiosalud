package pe.pucp.inf320.riesgocardiovascular;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.text.DecimalFormat;

import static java.lang.Float.parseFloat;


public class MainActivity extends ActionBarActivity {
    private Button btnIngresar;
    private EditText txtEdad;
    private EditText txtPeso;
    private EditText txtTalla;
    private EditText txtPreSis;
    private EditText txtPreDia;
    private Switch tglSexo;
    private Switch tglFuma;
    private Switch tglHipertension;
    private Switch tglAntecedentes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        txtEdad = (EditText) findViewById(R.id.txtEdad);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        txtTalla = (EditText) findViewById(R.id.txtTalla);
        txtPreSis = (EditText) findViewById(R.id.txtPreSis);
        txtPreDia = (EditText) findViewById(R.id.txtPreDia);
        tglSexo = (Switch) findViewById(R.id.chkSexo);
        tglFuma = (Switch) findViewById(R.id.chkFuma);
        tglHipertension = (Switch) findViewById(R.id.chkHip);
        tglAntecedentes = (Switch) findViewById(R.id.chkRieCar);
        //pb = (ProgressBar) findViewById(R.id.progressBar);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validaciones
                if (txtEdad.getText().toString().trim().equals("") || txtPreSis.getText().toString().trim().equals("") || txtPreDia.getText().toString().trim().equals("") || txtTalla.getText().toString().trim().equals("") || txtPeso.getText().toString().trim().equals("")){
                    if (txtEdad.getText().toString().trim().equals("")){
                        Toast.makeText(MainActivity.this, R.string.valEdadVacia, Toast.LENGTH_SHORT).show();
                    }
                    if (txtTalla.getText().toString().trim().equals("")){
                        Toast.makeText(MainActivity.this, R.string.valTallaVacia, Toast.LENGTH_SHORT).show();
                    }
                    if (txtPreSis.getText().toString().trim().equals("")){
                        Toast.makeText(MainActivity.this, R.string.valPreSisVacia, Toast.LENGTH_SHORT).show();
                    }
                    if (txtPreDia.getText().toString().trim().equals("")){
                        Toast.makeText(MainActivity.this, R.string.valPreDiaVacia, Toast.LENGTH_SHORT).show();
                    }
                    if (txtPeso.getText().toString().trim().equals("")){
                        Toast.makeText(MainActivity.this, R.string.valPesoVacio, Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    //Variables
                    Double edad = new Double(txtEdad.getText().toString());
                    Double preSis = new Double(txtPreSis.getText().toString());
                    Double preDia = new Double(txtPreDia.getText().toString());
                    Double peso = new Double(txtPeso.getText().toString());
                    Double talla = new Double(txtTalla.getText().toString());

                    if(edad > 120 || preSis > 200 || preSis < 30 || preDia < 30 || preDia > 200 || peso > 200 || talla > 200 || talla < 10){
                        if (edad > 120){
                            Toast.makeText(MainActivity.this, R.string.valEdad, Toast.LENGTH_SHORT).show();
                        }
                        if (preSis > 200 || preSis < 30){
                            Toast.makeText(MainActivity.this, R.string.valPreSis, Toast.LENGTH_SHORT).show();
                        }
                        if (preDia > 200 || preDia < 30){
                            Toast.makeText(MainActivity.this, R.string.valPreDia, Toast.LENGTH_SHORT).show();
                        }
                        if (peso > 200){
                            Toast.makeText(MainActivity.this, R.string.valPeso, Toast.LENGTH_SHORT).show();
                        }
                        if (talla > 200 || talla < 10){
                            Toast.makeText(MainActivity.this, R.string.valTalla, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else{
                        Double bpCoef;
                        if (!tglSexo.isChecked()) {
                            if (tglHipertension.isChecked()) {
                                bpCoef = 1.92672;
                            } else {
                                bpCoef = 1.85508;
                            }
                        }
                        else{
                            if (tglHipertension.isChecked()) {
                                bpCoef = 2.88267;
                            } else {
                                bpCoef = 2.81291;
                            }
                        }
                        Double beta;
                        if (edad < 30){
                            beta = Math.log(30);
                        }
                        else{
                            if (edad > 74){
                                beta = Math.log(74);
                            }
                            else{
                                beta = Math.log(edad);
                            }
                        }


                        //Calcular IMC
                        Double imc = new Double(txtPeso.getText().toString())/Math.pow(new Double(txtTalla.getText().toString())/100,2);

                        //Calcular Riesgo de enfermedad CV
                        Double riesgoCardio;
                        if (!tglSexo.isChecked()){
                            //Hombre
                            beta = beta * 3.11296;
                            if (tglFuma.isChecked()){
                                beta = beta + 0.70953;
                            }
                            beta = beta + Math.log(imc)*0.79277 + Math.log(preSis)*bpCoef;
                            riesgoCardio = (1 - (Math.pow(0.88431,(Math.exp(beta - 23.9388)))))*100;

                        }
                        else{
                            //Mujer
                            beta = beta * 2.72107;
                            if (tglFuma.isChecked()){
                                beta = beta + 0.61868;
                            }
                            beta = beta + Math.log(imc)*0.51125 + Math.log(preSis)*bpCoef;
                            riesgoCardio = (1 - (Math.pow(0.94833,(Math.exp(beta - 26.0145)))))*100;
                        }

                        if (tglAntecedentes.isChecked()) {
                            riesgoCardio = new Double(20);
                        }

                        //Calcular Hipertension
                        Double riesgoHiper;
                        beta = 22.949536 + edad*(-0.156412);
                        if (tglSexo.isChecked()){
                            beta = beta - 0.202933;
                        }
                        if (tglFuma.isChecked()){
                            beta = beta - 0.190731;
                        }

                        beta = beta + imc*(-0.03381) + preSis *(-0.059330) + preDia*(-0.128468) + preDia*edad*0.001624;

                        riesgoHiper = (1 - Math.exp(-Math.exp(((Math.log(4)-beta)/0.876925))))*100;
                        DecimalFormat df = new DecimalFormat("####0.00");
                        if (tglHipertension.isChecked()) {
                            riesgoHiper = new Double(100);
                        }

                        Intent i = new Intent("pe.pucp.inf320.riesgocardiovascular.RESULTACTIVITY");
                        i.putExtra("imc", df.format(imc));
                        i.putExtra("rieCar", df.format(riesgoCardio));
                        i.putExtra("rieHip", df.format(riesgoHiper));
                        startActivity(i);
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up btnIngresar, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
